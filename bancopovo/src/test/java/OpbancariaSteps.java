package br.ucsal.bes20182.testequalidade.bancopovo;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;


public class OpbancariaTest {
private ContaCorrente conta;
	
	@Given("aberto conta corrente")
	public void instanciarConta() {
		conta = new ContaCorrente();
	}

	@When("efetuado deposito no $valor reais")
	@When("efetuado depsito no $valor reais")
	public void informarDesposito(Double valor) {
		conta.depositar(valor);
	}

	@Then("meu saldo � de $saldo reais")
	public void verificarSituacaoSaldo(Double saldo) {
		Assert.assertEquals(saldo, conta.consultarSaldo());
	}


}
